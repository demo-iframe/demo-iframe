# Spring Boot Thymeleaf 

Project example: 

![spring-boot-thymeleaf-example](spring-boot-thymeleaf-example.png)

Database example project in `db/db.sql`

## Run Spring Boot application
```
mvn spring-boot:run
```

## Run with docker

```bash
docker-compose up --build -d
```

